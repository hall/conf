# packages

Build a [package](./packages) directly with

    nix build '.#${package}'

|                        |                      |
| ---------------------- | -------------------- |
| [cad](./cad)           | various design files |
| [keyboard](./keyboard) | mechanical keyboards |
| [website](./website)   | personal webpages    |
